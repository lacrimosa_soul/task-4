﻿using System;

namespace Calculator
{
    class Program
    {

        static string[] Operations = { "+", "-", "*", "/"};
        static string[] Commands = { "calc", "matrix", "history", "menu", "exit" };
        static string[] CalcCommands = { "y", "n", "menu" };
        static string history;

        static void Main(string[] args)
        {
            Boolean exit = false;
            DisplayMenu();
            while (!exit)
            {
                string command = GetCommand("Введите команду:", Commands);
                switch (command)
                {
                    case "calc":
                        NumbersCalculation(false, 0);
                        break;
                    case "matrix":
                        MatrixMultiplication();
                        break;
                    case "history":
                        Console.WriteLine(history);
                        break;
                    case "menu":
                        DisplayMenu();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }
            }

            Console.WriteLine("Работа завершена, нажмите любую клавишу:");
            Console.ReadKey();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("Калькулятор - главное меню");
            Console.WriteLine("Позволяет производить математические операции +, -, *, / над числами");
            Console.WriteLine("Позволяет производить умножение матриц");
            Console.WriteLine("Позволяет сохранять предыдущий результат и использовать в следующем вычислении");
            Console.WriteLine("Список первоначальных команд:");
            Console.WriteLine("calc - режим работы с числами");
            Console.WriteLine("matrix - режим работы с матрицами");
            Console.WriteLine("history - история вычислений");
            Console.WriteLine("menu - главное меню");
            Console.WriteLine("exit - завершение работы");
        }

        private static void NumbersCalculation(bool next, double prevResult)
        {
            double firstNumber = prevResult;
            if (!next)
            {
                firstNumber = GetNumber("Введите первое число : ", true);
            }
            
            double secondNumber = GetNumber("Введите второе число: ", true);
            string stringOperation = GetCommand(
                "Введите операцию: +, -, *, / :", Operations);
            double result = Calculate(firstNumber, secondNumber, stringOperation);
            Console.WriteLine("Продолжить вычисление далее (используя предыдущий результат) - введите y");
            Console.WriteLine("Начать новое вычисление - введите n");
            Console.WriteLine("Главное меню - введите menu");
            string command = GetCommand("Введите команду:", CalcCommands);
            switch (command)
            {
                case "y":
                    NumbersCalculation(true, result);
                    break;
                case "n":
                    NumbersCalculation(false, 0);
                    break;
                case "menu":
                    DisplayMenu();
                    break;
            }
        }

        private static double Calculate(double firstNumber, double secondNumber, string stringOperation)
        {
            double result = 0;
            switch (stringOperation)
            {
                case "+":
                    result = firstNumber + secondNumber;
                    break;
                case "-":
                    result = firstNumber - secondNumber;
                    break;
                case "*":
                    result = firstNumber * secondNumber;
                    break;
                case "/":
                    if (secondNumber == 0)
                    {
                        secondNumber = GetNumber("Деление на 0 невозможно, введите другое число", false);
                        return Calculate(firstNumber, secondNumber, stringOperation);
                    }
                    result = firstNumber / secondNumber;
                    break;
            }
            Console.WriteLine("Результат {0} {1} {2} = {3}", firstNumber, stringOperation, secondNumber, result);
            history = history + firstNumber + " " + stringOperation + " " + secondNumber + " = " + result + "\n";
            return result;
        }

        private static double[,] MatrixMultiplication()
        {
            Console.WriteLine("Размерность первой матрицы:");
            int Matrix1Rows = Convert.ToInt16(GetNumber("Введите количество строк первой матрицы:", false));
            int Matrix1Cols = Convert.ToInt16(GetNumber("Введите количество колонок первой матрицы:", false));
            
            Console.WriteLine("Размерность второй матрицы:");
            int Matrix2Rows = Convert.ToInt16(GetNumber("Введите количество строк второй матрицы:", false));
            int Matrix2Cols = Convert.ToInt16(GetNumber("Введите количество колонок второй матрицы:", false));
            if (Matrix1Cols != Matrix2Rows)
            {
                Console.WriteLine("Ошибка - количество колонок первой матрицы и количество строк второй матрицы не равны");
                Console.WriteLine("Введите значения заново");
                return MatrixMultiplication();
            } else
            {
                double[,] Matrix1 = EnterMatrix("Ввод элементов первой матрицы:", Matrix1Rows, Matrix1Cols);
                double[,] Matrix2 = EnterMatrix("Ввод элементов второй матрицы:", Matrix2Rows, Matrix2Cols);
                double[,] Result = MultiplyMatrix(Matrix1, Matrix2, Matrix1Rows, Matrix2Cols, Matrix1Cols);
                PrintMatrix("Результат умножения:", Result);
                return Result;
            }
        }

        private static double[,] EnterMatrix(string title, int MatrixRows, int MatrixCols)
        {
            double[,] Matrix = new double[MatrixRows, MatrixCols];
            Console.WriteLine(title);
            for (int i = 0; i < MatrixRows; i++)
            {
                Console.WriteLine("Заполнение строки {0}", i+1);
                for (int j = 0; j < MatrixCols; j++)
                {
                    Matrix[i, j] =  GetNumber("Введите значения колонки " + (j + 1) + ":", true);
                }
            }
            PrintMatrix("Введенная матрица", Matrix);
            return Matrix;
        }

        public static void PrintMatrix(string title, double[,] matrix)
        {
            Console.WriteLine(title);
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private static double[,] MultiplyMatrix(double[,] Matrix1, double[,] Matrix2, int Matrix1Rows, int Matrix2Cols, int Matrix1Cols)
        {
            double[,] result = new double[Matrix1Rows, Matrix2Cols];
            for (int i = 0; i < Matrix1Rows; i++)
            {
                for (int j = 0; j < Matrix2Cols; j++)
                {
                    result[i, j] = 0;
                    for (int k = 0; k < Matrix1Cols; k++)
                    {
                        result[i, j] = result[i, j] + Matrix1[i, k] * Matrix2[k, j];
                    }
                }
            }
            return result;
        }

        private static double GetNumber(string outputText, Boolean needZero)
        {
            double parse;
            Console.WriteLine(outputText);
            string tempInput = Console.ReadLine();
            if (!double.TryParse(tempInput, out parse))
            {
                Console.WriteLine("Неверное значение! Введите число:");
                return GetNumber(outputText, needZero);
            }
            if (!needZero && parse == 0)
            {
                Console.WriteLine("Значение должно быть больше 0:");
                return GetNumber(outputText, needZero);
            }
            return double.Parse(tempInput);
        }

        private static string GetCommand(string outputText, string[] commands)
        {
            Console.WriteLine(outputText);
            string tempInput = Console.ReadLine();
            while (!IsValidCommand(tempInput, commands))
            {
                Console.WriteLine("Неверная команда - {0}", tempInput);
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }

        private static bool IsValidCommand(string input, string[] available)
        {
            foreach (String command in available)
            {
                if (command.Equals(input))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
